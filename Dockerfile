FROM ubuntu

RUN apt-get update && apt-get install -y x11vnc xvfb chromium-browser
RUN mkdir ~/.vnc
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY policy.json /etc/chromium-browser/policies/managed/policy.json

ENV HOME /
CMD xvfb-run --server-args="-screen 0 1280x800x24 -ac +extension RANDR" /entrypoint.sh
