#!/bin/sh

chromium-browser --disable-web-security --user-data-dir=/root/ 'http://webapp:8888/auth/#login' &
x11vnc -forever -nopw -create &
VNCPID=$!
wait $VNCPID
